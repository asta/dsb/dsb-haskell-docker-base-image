# DSB Haskell Docker Base Image

Docker Image für GitLab CI/CD Pipelines, für die DSB Haskell Projekte.

Es gibt keine Garantie, dass dieses Image für andere Zwecke genutzt werden kann.


## In CI/CD Pipeline Nutzen

Beispiel `.gitlab-ci.yml` Datei:

```yaml
image: docker.gitlab.gwdg.de/asta/dsb/dsb-haskell-docker-base-image:latest

stages:
  - build


build-code:
  stage: build
  script:
    - cabal build
  artifacts:
    paths:
      - "./dist-newstyle/"

build-doc:
  stage: build
  script:
    - make docs
  artifacts:
    paths:
      - "./docs/"
```





