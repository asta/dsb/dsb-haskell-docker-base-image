#!/bin/bash

# Exit on error
set -e

# Print all commands
set -x

# Don't expect user input
export DEBIAN_FRONTEND=noninteractive


echo "Apt update"
apt-get update

echo "Install software"
#apt-get install -y --no-install-recommends openjdk-11-jdk openjdk-11-jdk-headless ant ant-optional git make wget curl build-essential locales
apt-get install -y --no-install-recommends git make wget curl build-essential locales


echo "Generate locales"
locale -a
locale-gen en_US.UTF-8
locale-gen de_DE.UTF-8
locale -a


echo "Install dependencies for projects"
cabal update
cat ~/.cabal/config
sed -i 's,  -- ghc-options:,  ghc-options:,;s, ghc-options:, ghc-options: -haddock ,' ~/.cabal/config
cat ~/.cabal/config

wget https://gitlab.gwdg.de/asta/dsb/shwf/-/raw/main/cabal.project
wget https://gitlab.gwdg.de/asta/dsb/datenschutzwebseite/-/raw/main/datenschutzwebseite.cabal
wget https://gitlab.gwdg.de/asta/dsb/shwf/-/raw/main/shwf.cabal
cat datenschutzwebseite.cabal
cat shwf.cabal
#cabal install --only-dependencies --cabal-file datenschutzwebseite.cabal
#cabal install --only-dependencies --cabal-file shwf.cabal

cat *.cabal | sed 's,--.*$,,g;s,  *, ,g;' | tr '\n' ' ' | sed 's,build-depends: \([^:]*\):,\nbuild-depends: \1:\n,g;s,build-depends: \([^:]*\)$,\nbuild-depends: \1:\n,g' | grep 'build-depends' | sed 's, [^ ]*:$,,' > depends.txt
cat depends.txt  | sed 's,^build-depends:, ,' | tr '\n' ',' | sed 's/,$//' | tr ',' '\n' | sed 's,^ *,,;s, *$,,g' | sort -u | grep -v 'datenschutzwebseite' | grep -v 'shwf'  > constraints.txt
#cat constraints.txt | cut -d '"' -f 2 | cut -d ' ' -f 1 | sort -u | grep -v 'datenschutzwebseite' | grep -v 'shwf' > packages.txt
rm -f *.cabal

cat constraints.txt

cat - <<EOF > dsb-haskell-docker-base-image.cabal
cabal-version:      3.0
name:               dsb-haskell-docker-base-image
version:            0.1.0.0

library
    default-extensions: OverloadedStrings
                      , DataKinds
                      , DeriveAnyClass
                      , DerivingVia
                      , DerivingStrategies
                      , GeneralizedNewtypeDeriving
                      , DuplicateRecordFields
                      , LambdaCase
                      , PatternSynonyms
    exposed-modules:  Yeet
    hs-source-dirs:   src
    default-language: GHC2021
    ghc-options:      -O0 -Wall
    build-depends:    base ^>=4.16.4.0
EOF
cat constraints.txt | sed 's/^/                    , /' >> dsb-haskell-docker-base-image.cabal
cabal install --only-dependencies --lib -v --reinstall --upgrade-dependencies

#cabal install --project-file="" $(cat constraints.txt  | grep -v ' -any' | tr '\n' ' ') $(cat packages.txt | tr '\n' ' ')
#cabal install --project-file="" $(cat packages.txt | tr '\n' ' ')


echo "Print information"
uname -a
ghc --version
cabal --version
#lsb_release -a




